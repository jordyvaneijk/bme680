# BME680

Python module that reads the Adafruit BME680 sensor and posts the readings to a sensor service REST API for monitoring.

# Prerequisites

Make sure the pi is running the latest version of Raspbian and Python 3:

```shell script
sudo apt-get update
sudo apt-get upgrade
sudo pip3 install --upgrade setuptools
# If above doesn't work try:
sudo apt-get install python3-pip
```

Also make sure I2C and SPI are enabled on the pi.

Also make sure the sensor service is running and accessible from the pi.

# Install guide

## Optionally, create a virtual environment:

```shell script
mkdir residential-climate-monitoring && cd residential-climate-monitoring
python3 -m venv .env
source .env/bin/activate
```

## Instal Python Libraries:

Install the following Python libraries: 

**Note:** If your default Python is version 3 you may need to run 'pip' instead. Just make sure you aren't trying to use 
CircuitPython on Python 2.x, it isn't supported!

* Install RPI.GPIO:
    This is a prerequisite for the Adafruit CircuitPython library.

    ```shell script
    pip3 install RPI.GPIO
    ```

* Install Adafruit Blinka:
    This will install the Adafruit CircuitPython library and all the other essential scripts.

    ```shell script
    pip3 install adafruit-blinka
    ```

* Install Adafruit Circuitpython BME680:
    This will install the drivers for the BME680 sensor.

    ```shell script
    pip3 install adafruit-circuitpython-bme680
    ```
  
* Install requests:
    Requests is a library used to communicate with the sensor-service.
    
    ```shell script
    pip3 install requests
    ```
  
## Install this script

//TODO: Publish the script as a nice python library to make it easier to install.

For now, the script is not yet available as a python library than can be installed from pypi. Instead the script will
have to be installed manually:

```shell script
git clone https://gitlab.com/residential-climate-monitoring/bme680.git
```

## Verify everything is working

Run blinkatest.py to check everything works:

```shell script
python3 rcm-bme680-reader/blinkatest.py
```

# Connect the sensor

To connect the sensor using I2C, wire it as follows:

|RPIO         |Sensor    |
|-------------|----------|
|3.3v         |VIN       |
|GND          |GND       |
|GPIO 2 (SDA) |SDI       |
|GPIO 3 (SCL) |SCK       |

By default the I2C address is 0x77, If you add a jumper from SDO to GND, the address will change to 0x76.

# Reading the sensor:

Once all the python libraries are installed, the blinkatest was sucessful and the sensor is connecetd it can be read using:

```shell script
cd ~/residential-climate-monitoring
source .env/bin/activate
python3 bme680/rcm-bme680-reader/readsensor.py --service-hostname=http://10.0.0.57:8080 --sensor-name=living-room
```

## Reading the sensor frequently on an automated basis

In order to gather readings from the sensor automatically over a longer period of time, crontab can be used.
The benefit of crontab is that it will trigger the script automatically on a regular interval based on a cron expression.
It will also keep working even if the raspberry pi is rebooted.

To configure it, enter:

```shell script
crontab -e
```  
This will open the config file in an editor and the cron expression plus the script to execute should be added.
To execute the script every minute add the following:
```shell script
* * * * * /home/pi/residential-climate-monitoring/.env/bin/python3 /home/pi/residential-climate-monitoring/bme680/rcm-bme680-reader/readsensor.py --service-hostname=<SENSOR_SERVICE_ADDRESS> --sensor-name=living-room
```  
Where `<SENSOR_SERVICE_ADDRESS>` should point to the sensor service + port. E.g. `http://10.0.0.57:8080`.
Note that it executes the script with python3 from the virtual env. This can be simplified to just `python3` when 
everything was installed globally on the system. 

To troubleshoot, the logs can be found at: `/home/pi/rcm.log`

## Sources:
* [Adafruit BME680](https://learn.adafruit.com/adafruit-bme680-humidity-temperature-barometic-pressure-voc-gas)
* [CircuitPython on Linux and Raspberry Pi](https://learn.adafruit.com/circuitpython-on-raspberrypi-linux)
* [Adafruit Blinka Library](https://pypi.org/project/Adafruit-Blinka/)
* [Enabling I2C](https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c)
* [Enabling SPI](https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-spi)
* [Raspberry GPIO](https://www.raspberrypi.org/documentation/usage/gpio/)
* [Crontab](https://www.raspberrypi.org/documentation/linux/usage/cron.md)
* [Requests library](https://requests.readthedocs.io/en/master/)
