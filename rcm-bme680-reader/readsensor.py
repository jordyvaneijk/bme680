import adafruit_bme680
import argparse
import busio
import board
import json
import logging
from logging.handlers import RotatingFileHandler
import requests

CONTENT_TYPE = 'application/json'
LOG_PATH = '/home/pi/rcm.log'
HEADERS = {'content-type': 'application/json'}


def init_logging():
    logger = logging.getLogger('sensor logger')
    # 4kb per minute = ~5.5mb/day
    handler = RotatingFileHandler(
        LOG_PATH, maxBytes=1048576, backupCount=5)
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    return logger


def init_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--service-hostname",
                        help="Hostname of the sensor service")
    parser.add_argument("--sensor-name",
                        help="Unique name for the sensor")
    return parser.parse_args()


def find_sensor_id():
    uri = f'{args.service_hostname}/sensors/search?name={args.sensor_name}'
    log.info('GET %s', uri)
    response = requests.get(uri)
    if response.status_code != 200:
        log.error('Failed to lookup sensors, reason: %s', response.text)
        quit()
    sensors = json.loads(response.text)['sensors']
    if len(sensors) == 0:
        return None
    if len(sensors) == 1:
        return sensors[0]['id']
    if len(sensors) > 1:
        log.error('Found more than 1 sensor, make sure the name is unique')
        quit()


def register_sensor():
    log.debug('Sensor not found, trying to register it...')
    uri = f'{args.service_hostname}/sensors'
    payload = {'name': args.sensor_name, 'type': 'BME680'}
    log.debug('POST %s, body: %s', uri, payload)
    response = requests.post(uri, json=payload, headers=HEADERS)
    if response.status_code is 200:
        sensor = json.loads(response.text)['sensor']
        log.info('Registered sensor with id: %s', sensor['id'])
        return sensor['id']
    else:
        log.error('Failed to register sensor, code=%s, message=%s',
                  response.status_code, response.text)
        quit()


def add_reading(reading):
    uri = f'{args.service_hostname}/sensors/{sensor_id}/readings'
    payload = {'reading': reading}
    log.debug('POST %s with body: %s', uri, payload)
    response = requests.post(uri, json=payload, headers=HEADERS)
    if response.status_code != 204:
        log.error('Failed to send sensor reading to service. Response: %s',
                  response.text)


def read_sensor():
    log.info('Reading BME80 sensor...')
    i2c = busio.I2C(board.SCL, board.SDA)
    bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c)
    temperature = bme680.temperature
    humidity = bme680.humidity
    pressure = bme680.pressure
    gas = bme680.gas
    reading = {
      'temperature': temperature,
      'humidity': humidity,
      'pressure': pressure,
      'gas': gas
    }
    log.info(f'Temperature: {temperature:.2f} *C, humidity: {humidity} %, '
             f'perssure: {pressure:.1f} hPa, gas: {gas} Ohms')
    add_reading(reading)


# Initialize logging so we know what is going on
log = init_logging()
# Parse the command line arguments
args = init_args()
# Do a look-up of the sensor id in the sensor-service
sensor_id = find_sensor_id()
# Register the sensor when sensor was not found
if sensor_id is None:
    sensor_id = register_sensor()
# Actually measure the values of the sensor and send them to the API
read_sensor()
